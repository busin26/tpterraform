provider "aws" {
  region     = "eu-west-1"
  access_key = ""
  secret_key = ""
}

resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "main"
  }
}

resource "aws_internet_gateway" "my_ig" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "main"
  }
}

resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_ig.id
  }

  tags = {
    Name = "Route table cours"
  }
}

resource "aws_subnet" "public_az1" {
  availability_zone = "eu-west-1a"
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.1.0/24"

}

resource "aws_subnet" "public_az2" {
  availability_zone = "eu-west-1b"
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.2.0/24"

}

resource "aws_subnet" "app_az1" {
  availability_zone = "eu-west-1a"
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.3.0/24"

}

resource "aws_subnet" "app_az2" {
  availability_zone = "eu-west-1b"
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.4.0/24"

}

resource "aws_subnet" "db_az1" {
  availability_zone = "eu-west-1a"
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.5.0/24"

}

resource "aws_subnet" "db_az2" {
  availability_zone = "eu-west-1b"
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.6.0/24"

}

resource "aws_main_route_table_association" "my_route_table_association" {
  vpc_id         = aws_vpc.my_vpc.id
  route_table_id = aws_route_table.my_route_table.id
}

resource "aws_security_group" "internet" {
  vpc_id      = aws_vpc.my_vpc.id

ingress {
    description      = ""
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.main.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "private" {
  vpc_id      = aws_vpc.my_vpc.id

ingress {
    description      = ""
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.main.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "data" {
  vpc_id      = aws_vpc.my_vpc.id

ingress {
    description      = ""
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.main.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_instance" "web-az2" {
  ami                    = "ami-02297540444991cc0"
  instance_type          = "t2.micro"
  availability_zone      = "eu-west-1b"
  subnet_id              = aws_subnet.private_app.1.id
  vpc_security_group_ids = [aws_security_group.public.id]

}

resource "aws_lb" "app" {
  name               = "ApplicationLoadBalancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.public.id]
  subnets            = [for subnet in aws_subnet.private_app : subnet.id]

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_listener" "app" {
  load_balancer_arn = aws_lb.app.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.front_end.arn
  }
}

resource "aws_lb_target_group" "front_end" {
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.my_vpc.id

  load_balancing_algorithm_type = "least_outstanding_requests"

  stickiness {
    enabled = true
    type    = "lb_cookie"
  }

  health_check {
    healthy_threshold   = 2
    interval            = 30
    protocol            = "HTTP"
    unhealthy_threshold = 2
  }

  depends_on = [
    aws_lb.app
  ]

  lifecycle {
    create_before_destroy = true
  }
}
